//
//  File 2.swift
//  
//
//  Created by Neshwa on 03/01/24.
//

import Foundation

//class 3
public struct SPM3 {
    var text3 = "Hello, world!"
    
    public init() {
    }
    
    public func hello3() -> String {
        return "Hello to you!"
    }
}
